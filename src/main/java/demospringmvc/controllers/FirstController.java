package demospringmvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.annotation.SessionScope;

import javax.servlet.http.HttpServletRequest;

@Component("NumberProvider")
//@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@SessionScope
class NumberProvider {
	int n = 0;

	public int getN() {
		int res = n;
		n++;
		return res;
	}
//	public void setN(int n) {
//		this.n = n;
//	}
}


@Controller
public class FirstController {

	NumberProvider nprovider;

	@Autowired
	public FirstController(NumberProvider np) {
		nprovider = np;
	}

	@GetMapping(value={"/hello", "/hello/{id}"})
	public String sayHello(HttpServletRequest request, Model model) {
		String msg = request.getParameter("msg");
		int n = nprovider.getN();

		model.addAttribute("msg", msg);
		model.addAttribute("counter", n);

		return "first/hello";
	}

	@GetMapping("/bye")
	public String sayBye(@RequestParam(value = "q", required = false) String q) {
		return "first/bye";
	}
}


