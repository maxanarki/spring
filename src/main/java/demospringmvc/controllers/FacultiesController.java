package demospringmvc.controllers;

import demospringmvc.Services.FacultyService;
import demospringmvc.models.Faculty;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@Controller
@RequestMapping("/faculties")
//@Validated
public class FacultiesController {
	private FacultyService service;

	public FacultiesController(FacultyService service) {
		this.service = service;
	}

	@GetMapping()
	public String index(Model model) {
		//return index(model, 1);
		return "redirect:faculties/page/0";
		//MessageHelper
	}


	@GetMapping("/page/{pn}")
	public String index(Model model,
						@PathVariable("pn") int pageNumber,
						HttpSession session,
						//HttpServletRequest request,
						@RequestParam(defaultValue = "") String sort, //defaultValue = "name"
						@RequestParam(defaultValue = "") String sortDir)
	{
		Helper.getPage(model, session, service, pageNumber, sort, sortDir);

		return "faculties/index"; // template name
	}


	@GetMapping("/{id}")
	public String show(@PathVariable("id") int id, Model model) {
		Faculty o = service.show(id).orElseThrow(IllegalArgumentException::new);
		model.addAttribute("o", o);
		model.addAttribute("title", "SHOW");
		return "faculties/show";
	}

	@GetMapping("/new")
	public String newUser(@ModelAttribute("o") Faculty o, Model model) {
		//model.addAttribute("user", new User());
		model.addAttribute("title", "NEW");
		return "faculties/new";
	}


	@GetMapping("/error")
	public String getDefaultPage(@RequestHeader(name = "errorcode") String errorCode) {
		//logger.info("error code : " + errorCode);
		return "faculties/error";
	}


	@PostMapping()
	public String create(@ModelAttribute("o") @Valid Faculty o, BindingResult br, Model model) {
		if (br.hasErrors()) {
			model.addAttribute("title", "Fix errors please");
			return "faculties/new";
		}
		service.save(o);
		return "redirect:/faculties";
	}


	@GetMapping("/{id}/edit")
	public String edit(Model model, @PathVariable("id") int id) {
		Faculty o = service.show(id).orElseThrow(IllegalArgumentException::new);
		model.addAttribute("o", o);
		model.addAttribute("title", "EDIT");
		return "faculties/edit";
	}

	@PatchMapping("/{id}")
	public String update(@ModelAttribute("user") @Valid Faculty o,
						 BindingResult br,
						 @PathVariable("id") int id,
						 Model model) {
		if (br.hasErrors()) {
			model.addAttribute("title", "Fix errors please");
			return "faculties/edit";
		}
		o.setId(id);
		service.save(o);
		return "redirect:/faculties";
	}


	@DeleteMapping("/{id}")
	public String delete(@PathVariable("id") int id) {
		service.delete(id);
		return "redirect:/faculties";
	}


}
