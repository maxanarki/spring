package demospringmvc.controllers;

import demospringmvc.Services.UserService;
import demospringmvc.jwt.JwtTokenProvider;
import demospringmvc.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class AuthController {
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	@Autowired
	private UserService userService;

	@GetMapping(value = {"", "/index"})
	public String viewHomePage() {
		return "index";
	}


	@GetMapping("/login")
	public String showLoginForm(Model model) {
		model.addAttribute("user", new User());
		return "auth/login_form";
	}


	@PostMapping("/process_login")
	public String processLogin(Model model, HttpSession session,
							   @Valid User user, BindingResult br) {
		if (br.hasErrors()) {
			ObjectError error = new ObjectError("globalError", "eeerror BindingResult");
			br.addError(error);
			return "auth/login_form";
		}

		try {
			String username = user.getName();
			String pwd = user.getPassword();
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, pwd));
			//User user = userService.findByUsername(username);

//			if (user == null) {
//				throw new UsernameNotFoundException("User with username: " + username + " not found");
//			}

			String token = jwtTokenProvider.createToken(username, user.getRoles());

//			Map<Object, Object> response = new HashMap<>();
//			response.put("username", username);
//			response.put("token", token);

			session.setAttribute("username", username);
			session.setAttribute("token", token);
		}
		catch (AuthenticationException ex) {
			ObjectError error = new ObjectError("globalError",
					"processLogin: AuthenticationException: Wrong password");
			br.addError(error);
			return "auth/login_form";
		}
		model.addAttribute("user", new User());

		return "auth/login_success";
	}



	@GetMapping("/register")
	public String showRegistrationForm(Model model) {
		model.addAttribute("user", new User());

		return "auth/reg_form";
	}

	@PostMapping("/process_register")
	public String processRegister(Model model, @Valid User user, BindingResult br) {
		if (br.hasErrors()) {
			ObjectError error = new ObjectError("globalErrorQQ", "eeeeerror");
			br.addError(error);
			return "auth/reg_form";
		}

		try {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String encodedPassword = passwordEncoder.encode(user.getPassword());
			user.setPassword(encodedPassword);
			userService.save(user);
		}
		catch (Exception ex) {
			ObjectError error = new ObjectError("globalError", "The user already exists");
			br.addError(error);
			return "auth/reg_form";
		}
		model.addAttribute("user", new User());

		return "auth/reg_success";
	}

}
