package demospringmvc.controllers;

import demospringmvc.Services.UserService;
import demospringmvc.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@Controller
@RequestMapping("/users")
//@Validated
public class UsersController {
	private UserService userService;

	public UsersController(UserService usersDAO) {
		this.userService = usersDAO;
	}

	@GetMapping()
	//@PreAuthorize("hasRole('ADMIN')")
	public String index(Model model) {
		//return index(model, 1);
		return "redirect:/users/page/0";
		//MessageHelper
	}


	@GetMapping("/page/{pn}")
	//@PreAuthorize("hasRole('ADMIN')")
	public String index(Model model,
						HttpSession session,
						//HttpServletRequest request,
						@PathVariable("pn") int pageNumber,
						@RequestParam(defaultValue = "name") String sort, //defaultValue = "name"
						@RequestParam(defaultValue = "") String sortDir) {
		//String msg = request.getParameter("msg");
		//Pageable firstPageWithTwoElements = PageRequest.of(0, 2);

		Helper.getPage(model, session, userService, pageNumber, sort, sortDir);

		return "users/index";
	}


	@GetMapping("/{id}")
	public String show(@PathVariable("id") int id, Model model) {
		User user = userService.show(id)
				.orElseThrow(IllegalArgumentException::new);
		model.addAttribute("user", user);
		model.addAttribute("title", "SHOW");
		return "users/show";
	}

	@GetMapping("/new")
	public String newUser(@ModelAttribute("user") User user, Model model) {
		//model.addAttribute("user", new User());
		model.addAttribute("title", "NEW");
		return "users/new";
	}


	@GetMapping("/error")
	public String getDefaultPage(@RequestHeader(name = "errorcode") String errorCode) {
		//logger.info("error code : " + errorCode);
		return "users/error";
	}


	@PostMapping()
	public String create(@ModelAttribute("user") @Valid User user, BindingResult br, Model model) {
		if (br.hasErrors()) {
			model.addAttribute("title", "Fix errors please");
			return "users/new";
		}
		userService.save(user);
		return "redirect:/users";
	}

	@GetMapping("/{id}/edit")
	public String edit(Model model, @PathVariable("id") int id) {
		User user = userService.show(id).orElseThrow(IllegalArgumentException::new);
		model.addAttribute("user", user);
		model.addAttribute("title", "EDIT");
		return "users/edit";
	}

	@PatchMapping("/{id}")
	public String update(@ModelAttribute("user") @Valid User user,
						 BindingResult br,
						 @PathVariable("id") long id,
						 Model model) {
		if (br.hasErrors()) {
			model.addAttribute("title", "Fix errors please");
			return "users/edit";
		}
		user.setId(id);
		userService.save(user);
		return "redirect:/users";
	}


	@DeleteMapping("/{id}")
	public String delete(@PathVariable("id") int id) {
		userService.delete(id);
		return "redirect:/users";
	}


}
