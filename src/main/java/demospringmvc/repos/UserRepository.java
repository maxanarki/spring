package demospringmvc.repos;

import demospringmvc.models.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {
	Optional<User> findByName(String name);
}

