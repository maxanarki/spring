package demospringmvc.models;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.bytebuddy.implementation.bind.annotation.Default;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name="faculties")
public class Faculty {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

//	public int getId() {
//		return id;
//	}
//
//	public boolean setId(int i) {
//		id = i;
//		return true;
//	}

	//@NotEmpty()
	//@NotNull(message = "Name may not be null........")
	//@Size(min=3, max=20, message = "sizeeee")
	//@NotEmpty(message = "Name may not be empty.")
	//@Column(columnDefinition = "TEXT")
	private String name;

	private int budget = 1;
	private int contract = 1;

//	public Faculty(String fname, int budget, int contract) {
//		this.fname = fname;
//		this.budget = budget;
//		this.contract = contract;
//	}

	@OneToMany()
	private Set<User> users;
}