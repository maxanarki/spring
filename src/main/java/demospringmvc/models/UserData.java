package demospringmvc.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@SuperBuilder
//@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name="userdata")
public class UserData {

	public UserData() {
//		User u = new User();
//		u?.udata = this;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	String realdata;

//	@OneToOne(mappedBy = "udata", cascade = CascadeType.ALL)
//	private User user;
}
