package demospringmvc.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="users", indexes = {
		@Index(name = "name", columnList = "name", unique = true)})
public class User extends BaseEntity {
//	@Id
//	//@GeneratedValue(strategy = GenerationType.AUTO)
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private int id;

	//@NotEmpty()
	@NotNull(message = "* Name may not be null........")
	@Size(min=10, max=20, message = "sizeeee")
	@NotEmpty(message = "* Name may not be empty.")
	//@Column(columnDefinition = "TEXT")
	private String name;

	@NotEmpty(message = "* Pwd may not be empty.")
	private String password;

	@OneToOne(cascade = CascadeType.ALL)
	private UserData udata;

	@OneToOne(cascade = CascadeType.ALL)
	private Faculty faculty;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_roles",
			joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
	private List<Role> roles;
}
