package demospringmvc.Services;

import demospringmvc.models.Faculty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;


interface FacultyRepository extends PagingAndSortingRepository<Faculty, Integer> {
}

@Component
public class FacultyService implements MyService<Faculty>{

	@Autowired
	private FacultyRepository rep;


	public Page<Faculty> getPage(Pageable page) {
		return rep.findAll(page);
	}


	public Optional<Faculty> show(int id) {
//		Optional<User> u = Optional.ofNullable(null);
//		u.filter()

		return rep.findById(id);
	}


	public Faculty save(Faculty o) {
		return rep.save(o);
	}


//	@Deprecated
//	public boolean update(User user) {
////		try {
////			PreparedStatement statement = conn.prepareStatement(
////					"UPDATE users SET name = ? WHERE id = ?");
////			statement.setString(1, user.getName());
////			statement.setInt(2, user.getId());
////			statement.executeUpdate();
////			return true;
////		} catch (SQLException e) {
////			e.printStackTrace();
////		}
//		return false;
//	}


	public void delete(int id) {
		rep.deleteById(id);
	}

}
