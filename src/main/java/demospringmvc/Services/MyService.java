package demospringmvc.Services;

import demospringmvc.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface MyService<T> {
	public Page<T> getPage(Pageable page);
	Optional<T> show(int id);
	T save(T o);
	void delete(int id);
}
