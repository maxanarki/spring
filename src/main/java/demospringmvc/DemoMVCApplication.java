package demospringmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;


@Configuration
//@ComponentScan("demospringmvc")
//@EnableWebMvc // -------------- Надо выключить чтобы работали статик ресурсы
class AppConfig implements WebMvcConfigurer {

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		//slr.setDefaultLocale(Locale.US);
		return slr;
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}


	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}

//	@Bean
//	public NumberProvider qqq() {
//		return new NumberProvider();
//	}


//	public LayoutDialect layoutDialect() {
//		TemplateEngine templateEngine = new TemplateEngine();
//		templateEngine.addDialect(new LayoutDialect());
//
//		return new LayoutDialect();
//	}

//	private static final String VIEWS = "/resources/templates/"; //""/WEB-INF/views/";
//
//	@Bean
//	public ITemplateResolver templateResolver() {
//		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
//		resolver.setPrefix(VIEWS);
//		resolver.setSuffix(".html");
//		resolver.setTemplateMode(TemplateMode.HTML);
//		resolver.setCharacterEncoding("UTF-8");
//		resolver.setCacheable(false);
//		return resolver;
//	}
//
//	@Bean
//	public SpringTemplateEngine templateEngine() {
//		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//		templateEngine.addTemplateResolver(new UrlTemplateResolver());
//		templateEngine.addTemplateResolver(templateResolver());
//		templateEngine.addDialect(new SpringSecurityDialect());
//		templateEngine.addDialect(new LayoutDialect());
//		templateEngine.addDialect(new Java8TimeDialect());
//		return templateEngine;
//	}
}


//@Component
//class AppStartupRunner implements ApplicationRunner {
////	private static final Logger LOG =
////			LoggerFactory.getLogger(AppStartupRunner.class);
//
//	public static int counter;
//
//	@Override
//	public void run(ApplicationArguments args) throws Exception {
////		LOG.info("Application started with option names : {}",
////				args.getOptionNames());
////		LOG.info("Increment counter");
//		counter++;
//	}
//}


//@Component
//class CommandLineAppStartupRunner implements CommandLineRunner {
////	private static final Logger LOG =
////			LoggerFactory.getLogger(CommandLineAppStartupRunner.class);
//
//	public static int counter;
//
//	@Override
//	public void run(String...args) throws Exception {
//		//LOG.info("Increment counter");
//		counter++;
//	}
//}


//@Component
//class StartupApplicationListenerExample implements ApplicationListener<ContextRefreshedEvent> {
//	//private static final Logger LOG
//	//		= Logger.getLogger(StartupApplicationListenerExample.class);
//
//	@Autowired
//	private Environment environment;
//
//	public static int counter;
//
//	@Override public void onApplicationEvent(ContextRefreshedEvent event) {
//		//LOG.info("Increment counter");
//		AnnotationConfigServletWebServerApplicationContext context
//				= (AnnotationConfigServletWebServerApplicationContext)event.getSource();
//
//		//
//		// ERROR LIFECYCLESTATE
//		//
//		//ServletContext
////		ServletContext c = context.getServletContext();
////		FilterRegistration.Dynamic d = c.addFilter("hiddenHttpMethodFilter", new HiddenHttpMethodFilter());
////		d.addMappingForUrlPatterns(null ,true, "/*");
//
//		counter++;
//	}
//}

//class qqq extends SpringServletContainerInitializer {
//	@Override
//	public void onStartup(Set<Class<?>> webAppInitializerClasses, ServletContext servletContext) throws ServletException {
//		super.onStartup(webAppInitializerClasses, servletContext);
//	}
//}


//class qqqq extends AbstractAnnotationConfigDispatcherServletInitializer {
//
//	@Override
//	protected Class<?>[] getRootConfigClasses() {
//		return new Class[0];
//	}
//
//	@Override
//	protected Class<?>[] getServletConfigClasses() {
//		return new Class[0];
//	}
//
//	@Override
//	protected String[] getServletMappings() {
//		return new String[0];
//	}
//
//	@Override
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		super.onStartup(servletContext);
//
//		servletContext
//				.addFilter("hiddenHttpMethodFilter", new HiddenHttpMethodFilter())
//				.addMappingForUrlPatterns(null ,true, "/*");
//	}
//}


//class MySpringMvcDispatcherSerlvetIntitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
//	@Override
//	protected Class<?>[] getRootConfigClasses() {
//		return null;
//	}
//
//	@Override
//	protected Class<?>[] getServletConfigClasses() {
//		return new Class[]{AppConfig.class};
//	}
//
//	@Override
//	protected String[] getServletMappings() {
//		return new String[]{"/"};
//	}
//
//	@Override
//	public void onStartup(ServletContext aServletContext) throws ServletException {
//		super.onStartup(aServletContext);
//		registerHiddenFieldFilter(aServletContext);
//	}
//
//	private void registerHiddenFieldFilter(ServletContext aContext) {
//		aContext.addFilter("hiddenHttpMethodFilter",
//				new HiddenHttpMethodFilter()).addMappingForUrlPatterns(null ,true, "/*");
//	}
//}
//
//@Configuration
//class WebAppInitializer2 implements WebApplicationInitializer {
//
//	@Override
//	public void onStartup(ServletContext aServletContext) throws ServletException
//	{
//		registerHiddenFieldFilter(aServletContext);
//	}
//
//	private void registerHiddenFieldFilter(ServletContext aContext) {
//		aContext.addFilter("hiddenHttpMethodFilter", new     HiddenHttpMethodFilter())
//				.addMappingForUrlPatterns(null ,true, "/*");
//	}
//}


@Configuration
class Initializer2 implements ServletContextInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		//System.err.println("------------------------------------");

		//ServletContext c = context.getServletContext();
		FilterRegistration.Dynamic d = servletContext
						.addFilter("hiddenHttpMethodFilter", new HiddenHttpMethodFilter());
		d.addMappingForUrlPatterns(null ,true, "/*");
	}

}



@SpringBootApplication//(exclude = {ErrorMvcAutoConfiguration.class})
public class DemoMVCApplication{

//	@Override
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		System.out.println("--------onStartup--------");
//		super.onStartup(servletContext);
//	}

	public static void main(String[] args) {
		System.out.println("111111----1111111");
		//SpringApplication.run(DemoMVCApplication.class, args);
		SpringApplication.run(new Class[] {
				//MySpringMvcDispatcherSerlvetIntitializer.class,
				DemoMVCApplication.class }, args);
		System.out.println("2222----2222");
	}


//	WebMvcConfigurer webMvcConfigurer() {
//		return WebMvcConfigurerAdapter
//	}


//	Unlike what the spring-boot states, to get my spring-boot jar to serve the content:
//	I had to add specifically register my src/main/resources/static content through this config class:
//
//	@Configuration
//	public class StaticResourceConfiguration implements WebMvcConfigurer {
//
//		private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
//				"classpath:/META-INF/resources/", "classpath:/resources/",
//				"classpath:/static/", "classpath:/public/" };
//
//		@Override
//		public void addResourceHandlers(ResourceHandlerRegistry registry) {
//			registry.addResourceHandler("/**")
//					.addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
//		}
//	}

}
